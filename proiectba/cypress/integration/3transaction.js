
describe('Big transaction. Big money. Big data.', () => {
    it('Big Brain', () => {
      const account = Cypress.env("account")
      cy.visit('https://parabank.parasoft.com/parabank/index.htm')
      cy.get('input[name="username"]').type(account)
      cy.get('input[name="password"]').type(account)
      cy.get('form').submit()
      
      cy.wait(100)
      cy.get('div#leftPanel > ul > li:nth-child(3) > a')
      .should('have.attr', 'href')
      .then((href) => {
          cy.visit("https://parabank.parasoft.com" + href)
      })
      cy.wait(200)
      cy.get('p > input[id="amount"').type(1337)
      cy.get('div > input[class="button"').click()

      cy.wait(200)
      cy.get('p > span[id="amount"').should("have.text", '$1337.00')
    })
  })
  