describe('Requests a small loan of a million dollars', () => {
    it('and builds a wall because it gets rejected', () => {
      const account = Cypress.env("account")
      cy.visit('https://parabank.parasoft.com/parabank/index.htm')
      cy.get('input[name="username"]').type(account)
      cy.get('input[name="password"]').type(account)
      cy.get('form').submit()
      
      // first request - denied
      cy.wait(100)
      cy.get('div#leftPanel > ul > li:nth-child(7) > a')
      .should('have.attr', 'href')
      .then((href) => {
          cy.visit("https://parabank.parasoft.com" + href)
      })
      cy.wait(200)
      cy.get('tr > td > input[id="amount"]').type(1000000)
      cy.get('tr > td > input[id="downPayment"').type(1337)
      cy.wait(200)

      cy.get('form').submit()
      cy.wait(200)

      cy.get('tbody > tr > td[id="loanStatus"]').should('have.text', 'Denied')

      // second request - denied
      cy.wait(100)
      cy.get('div#leftPanel > ul > li:nth-child(7) > a')
      .should('have.attr', 'href')
      .then((href) => {
          cy.visit("https://parabank.parasoft.com" + href)
      })
      cy.wait(200)
      cy.get('tr > td > input[id="amount"]').type(2000000)
      cy.get('tr > td > input[id="downPayment"').type(1337)
      cy.wait(200)

      cy.get('form').submit()
      cy.wait(200)

      cy.get('tbody > tr > td[id="loanStatus"]').should('have.text', 'Denied')

      // third request - approved

      cy.wait(100)
      cy.get('div#leftPanel > ul > li:nth-child(7) > a')
      .should('have.attr', 'href')
      .then((href) => {
          cy.visit("https://parabank.parasoft.com" + href)
      })
      cy.wait(200)
      cy.get('tr > td > input[id="amount"]').type(100)
      cy.get('tr > td > input[id="downPayment"').type(30)
      cy.wait(200)

      cy.get('form').submit()
      cy.wait(200)

      cy.get('tbody > tr > td[id="loanStatus"]').should('have.text', 'Approved')

      // logout
      cy.get('div#leftPanel > ul > li:nth-child(8) > a')
     .should('have.attr', 'href')
     .then((href) => {
       cy.visit("https://parabank.parasoft.com" + href)
     })
    cy.url().should('equal', 'https://parabank.parasoft.com/parabank/index.htm?ConnType=JDBC')
    })
  })
  