
describe('Requests a L33T loan', () => {
    it('and builds a 360 noscopes everyone', () => {
      const account = Cypress.env("account")
      cy.visit('https://parabank.parasoft.com/parabank/index.htm')
      cy.get('input[name="username"]').type(account)
      cy.get('input[name="password"]').type(account)
      cy.get('form').submit()
      
      cy.wait(100)
      cy.get('div#leftPanel > ul > li:nth-child(7) > a')
      .should('have.attr', 'href')
      .then((href) => {
          cy.visit("https://parabank.parasoft.com" + href)
      })
      cy.wait(200)
      cy.get('tr > td > input[id="amount"]').type(1337)
      cy.get('tr > td > input[id="downPayment"').type(420)
      cy.wait(200)

      cy.get('form').submit()
      cy.wait(200)

      cy.get('tbody > tr > td[id="loanStatus"]').should('have.text', 'Approved')
      
    })
  })
  