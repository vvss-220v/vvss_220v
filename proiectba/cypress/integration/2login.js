describe('Testing the login system', () => {
  it('and prays for the best', () => {
    const account = Cypress.env("account")
    cy.visit('https://parabank.parasoft.com/parabank/index.htm')
    cy.get('input[name="username"]').type(account)
    cy.get('input[name="password"]').type(account)
    cy.get('form').submit()
    cy.url().should('equal', 'https://parabank.parasoft.com/parabank/overview.htm')
    cy.get("[class=smallText]").should('have.text', 'Welcome ' + account + ' ' + account)
    cy.get('div#leftPanel > ul > li:nth-child(8) > a')
     .should('have.attr', 'href')
     .then((href) => {
       cy.visit("https://parabank.parasoft.com" + href)
     })
    cy.url().should('equal', 'https://parabank.parasoft.com/parabank/index.htm?ConnType=JDBC')
  })
})
