package tasks.utils;

public class Constants {
    public static final String DESCRIPTION_ERROR = "Description cannot be empty";
    public static final String INTERVAL_ERROR = "Interval should be >= 1";
    public static final String TIME_ERROR = "Time is bad.";
    public static final String START_DATE_ERROR = "Start date must be after 01/01/2022";
    public static final String DATES_ERROR = "End must be after start date";
}
