package tasks.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public static Date NewDate(int day, int month, int year)  {

        String date_string = String.format("%d-%d-%d", day, month, year);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        try {
            return formatter.parse(date_string);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
