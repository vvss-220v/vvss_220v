package tasks.model;

import javafx.collections.ObservableList;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static tasks.utils.Constants.DATES_ERROR;
import static tasks.utils.Constants.START_DATE_ERROR;

public class TasksOperations {
    public ArrayList<Task> tasks;
    public TasksOperations(ObservableList<Task> tasksList){
        tasks=new ArrayList<>();
        tasks.addAll(tasksList);
    }
    public Iterable<Task> incoming(Date start, Date end) throws Exception {
        System.out.println(start);
        System.out.println(end);
        if(start.after(end)) {
            throw new Exception(DATES_ERROR);
        }
        if(start.before(Date.from(LocalDateTime.of(2022,1,1,0,0,0).atZone(ZoneId.of("Europe/Bucharest")).toInstant()))) {
            throw new Exception(START_DATE_ERROR);
        }
        ArrayList<Task> incomingTasks = new ArrayList<>();
        for (Task t : tasks) {
            Date nextTime = t.nextTimeAfter(start);
            if (nextTime != null && (nextTime.before(end) || nextTime.equals(end))) {
                incomingTasks.add(t);
                System.out.println(t.getTitle());
            }
            else{
                System.out.println("bad");
            }
        }
        return incomingTasks;
    }
}

