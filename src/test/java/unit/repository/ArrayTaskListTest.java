package unit.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tasks.model.ArrayTaskList;
import tasks.model.Task;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArrayTaskListTest {
    ArrayTaskList repo;
    @Mock
    Task task;
    @Mock
    Task task2;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        repo = new ArrayTaskList();
    }

    @Test
    void AddTaskTest() {
        repo.add(task);
        assertEquals(1, repo.size());
    }

    @Test
    void RemoveTaskTest() {
        repo.add(task);
        repo.add(task2);
        assertEquals(2, repo.size());

        repo.remove(task2);
        assertEquals(1, repo.size());
    }
}
