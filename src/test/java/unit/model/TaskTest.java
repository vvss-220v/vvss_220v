package unit.model;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tasks.model.Task;
import tasks.utils.Constants;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {
    @Test
    @Tag("ECP")
    @DisplayName("Create a valid task")
    void create_valid_task_ecp() {
        Task task = null;
        try {
            task = new Task("descriere", Task.getDateFormat().parse("2022-03-09 19:00"), Task.getDateFormat().parse("2022-03-09 22:00"), 3);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assertNotEquals(null, task);
    }

    @Test
    @Tag("ECP")
    @Timeout(2)
    void create_invalid_task_description_ecp() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            var task = new Task("", Task.getDateFormat().parse("2022-03-09 19:00"), Task.getDateFormat().parse("2022-03-09 22:00"), 3);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(Constants.DESCRIPTION_ERROR));
    }

    @Test
    @Tag("ECP")
    @DisplayName("Create a valid task")
    void create_valid_task_interval_ecp(){

        Task task = null;
        try {
            task = new Task("descriere", Task.getDateFormat().parse("2022-03-09 19:00"), Task.getDateFormat().parse("2022-03-09 22:00"), 10);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assertNotEquals(null, task);
    }

    @Test
    @Tag("ECP")
    @Timeout(2)
    void create_invalid_task_interval_ecp() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            var task = new Task("descriere", Task.getDateFormat().parse("2022-03-09 19:00"), Task.getDateFormat().parse("2022-03-09 22:00"), -7);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.equals(Constants.INTERVAL_ERROR));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, Integer.MAX_VALUE-1, Integer.MAX_VALUE})
    @Tag("BVA")
    void create_valid_task_interval_BVA(int number) {
        Task task = null;
        try {
            task = new Task("descriere", Task.getDateFormat().parse("2022-03-09 19:00"), Task.getDateFormat().parse("2022-03-09 22:00"), number);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assertNotEquals(null, task);
    }

    @Test
    @Tag("BVA")
    void create_invalid_task_start_date_BVA() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            var task = new Task("descriere", Task.getDateFormat().parse("2021-12-31 19:00"), Task.getDateFormat().parse("2022-03-09 22:00"), 5);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(Constants.TIME_ERROR));
    }

    @Test
    @Tag("BVA")
    void create_valid_task_start_date_BVA() {
        Task task = null;
        try {
            task = new Task("descriere", Task.getDateFormat().parse("2022-01-02 19:00"), Task.getDateFormat().parse("2022-03-09 22:00"), 5);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assertNotEquals(null, task);
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, 0})
    @Tag("BVA")
    void create_invalid_task_interval_BVA(int number) {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            var task = new Task("descriere", Task.getDateFormat().parse("2022-03-09 19:00"), Task.getDateFormat().parse("2022-03-09 22:00"), number);
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(Constants.INTERVAL_ERROR));
    }
}