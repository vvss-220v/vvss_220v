package unit.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.model.Task;

import java.text.ParseException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TaskUnitTest {

    private Date startDate, endDate;
    private Task task;
    private Integer interval;
    private String title;
    private final boolean active = true;

    @BeforeEach
    void beforeEach() throws ParseException {
        startDate = Task.getDateFormat().parse("2022-02-12 10:10");
        endDate = Task.getDateFormat().parse("2022-02-13 10:10");
        title = "Title";
        interval = 1;
        task = new Task(title, startDate, endDate, interval);
        task.setActive(active);
    }

    @AfterEach
    void afterEach(){
        startDate = null;
        endDate = null;
        title = null;
        interval = null;
        task = null;
    }

    @Test
    void setActive() {
        task.setActive(true);
        assertTrue(task.isActive());
    }

    @Test
    void isRepeated() {
        assertTrue(task.isRepeated());
    }

    @Test
    void getStartTime() {
        assertEquals(startDate, task.getStartTime());
    }

    @Test
    void getEndTime() {
        assertEquals(endDate, task.getEndTime());
    }

    @Test
    void getTitle() {
        assertEquals(title, task.getTitle());
    }

    @Test
    void getRepeatInterval() {
        assertEquals(interval, task.getRepeatInterval());
    }
}
