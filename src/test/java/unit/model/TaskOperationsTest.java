package unit.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import tasks.model.Task;
import tasks.model.TasksOperations;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static tasks.utils.Constants.DATES_ERROR;
import static tasks.utils.Constants.START_DATE_ERROR;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TaskOperationsTest {
    TasksOperations tasksOperations;
    ObservableList<Task> tasksObservable;
    List<Task> tasksList;

    @BeforeAll
    void init() {
        Task task = new Task("title",
                Date.from(LocalDateTime.of(2022, 2,1,0,0,0).atZone(ZoneId.of("Europe/Bucharest")).toInstant()),
                Date.from(LocalDateTime.of(2022, 2,18,0,0,0).atZone(ZoneId.of("Europe/Bucharest")).toInstant()),
                3);

        task.setActive(true);
        Task task2 = new Task("title",
                Date.from(LocalDateTime.of(2023, 2,1,0,0,0).atZone(ZoneId.of("Europe/Bucharest")).toInstant()),
                Date.from(LocalDateTime.of(2023, 2,18,0,0,0).atZone(ZoneId.of("Europe/Bucharest")).toInstant()),
                3);

        task2.setActive(true);
        this.tasksList = new ArrayList<>();
        this.tasksList.add(task);
        this.tasksList.add(task2);
        this.tasksObservable = FXCollections.observableArrayList(tasksList);
        this.tasksOperations = new TasksOperations(tasksObservable);
    }
    int getSize(Iterable<Task> tasks){
        return (int) StreamSupport.stream(tasks.spliterator(), false).count();
    }

    // TC 1
    @Test
    void incomingTestDatesValid() {
        String err = "";
        Iterable<Task> incomingTasks = new ArrayList<>();
        try {
            incomingTasks = this.tasksOperations.incoming(
                    Date.from(LocalDateTime.of(2022, 1,2,0,0,0).atZone(ZoneId.of("Europe/Bucharest")).toInstant()),
                    Date.from(LocalDateTime.of(2022, 12,31,0,0,0).atZone(ZoneId.of("Europe/Bucharest")).toInstant()));

        } catch (Exception e) {

            err = e.getMessage();
        }
        assert (err.isEmpty());
        assert (getSize(incomingTasks) == 1);
    }

    // TC 2
    @Test
    void incomingTestStartDateInvalid() {
        Exception exception = assertThrows(Exception.class, () -> {
            this.tasksOperations.incoming(
                    Date.from(LocalDateTime.of(2021, 1,2,0,0,0).atZone(ZoneId.of("Europe/Bucharest")).toInstant()),
                    Date.from(LocalDateTime.of(2022, 12,31,0,0,0).atZone(ZoneId.of("Europe/Bucharest")).toInstant()));
        });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(START_DATE_ERROR));
    }

    // TC 3
    @Test
    void incomingTestOrderDatesInvalid() {Exception exception = assertThrows(Exception.class, () -> {
        this.tasksOperations.incoming(
                Date.from(LocalDateTime.of(2022, 12,2,0,0,0).atZone(ZoneId.of("Europe/Bucharest")).toInstant()),
                Date.from(LocalDateTime.of(2022, 1,31,0,0,0).atZone(ZoneId.of("Europe/Bucharest")).toInstant()));
    });

        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(DATES_ERROR));
    }

}
