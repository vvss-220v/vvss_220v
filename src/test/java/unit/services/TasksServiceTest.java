package unit.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.services.TasksService;
import tasks.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TasksServiceTest {
    @Mock
    ArrayTaskList repoMock;

    @Mock
    Task task;
    @Mock
    Task task2;

    TasksService service;

    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        service = new TasksService(repoMock);
    }

    @Test
    void GetObservableListTest(){
        var array = new ArrayList<Task>();
        array.add(task);
        when(repoMock.getAll()).thenReturn(array);

        var result = service.getObservableList();

        verify(repoMock).getAll();

        assert result.size() == 1;
        assert result.contains(task);
    }

    @Test
    void FilterTasksTest() throws Exception {
        when(task.getStartTime()).thenReturn(DateUtils.NewDate(10,5,2022));
        when(task.getEndTime()).thenReturn(DateUtils.NewDate(11,5,2022));
        when(task.nextTimeAfter(any(Date.class))).thenReturn(DateUtils.NewDate(11,5,2022));
        when(task2.getStartTime()).thenReturn(DateUtils.NewDate(13,5,2022));
        when(task2.getEndTime()).thenReturn(DateUtils.NewDate(14,5,2022));
        when(task2.nextTimeAfter(any(Date.class))).thenReturn(DateUtils.NewDate(14,5,2022));

        var array = new ArrayList<Task>();
        array.add(task);
        array.add(task2);
        when(repoMock.getAll()).thenReturn(array);

        var result = service.filterTasks(DateUtils.NewDate(9,5,2022), DateUtils.NewDate(12,5,2022));

        verify(repoMock).getAll();

        var resultList = StreamSupport.stream(result.spliterator(), false)
                .collect(Collectors.toList());

        assert resultList.size() == 1;
        assert resultList.contains(task);
    }

}
