package integration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.services.TasksService;
import tasks.utils.DateUtils;

import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ServiceRepositoryIntegrationTest {
    ArrayTaskList repo;
    TasksService service;

    @Mock
    Task task;
    @Mock
    Task task2;


    @BeforeEach
    void init() {
        MockitoAnnotations.initMocks(this);
        repo = new ArrayTaskList();
        service = new TasksService(repo);
    }

    @Test
    void GetObservableListTest(){
        repo.add(task);

        var result = service.getObservableList();

        assertEquals(1, result.size());
        assertTrue(result.contains(task));
    }

    @Test
    void AddTask(){
        var date =DateUtils.NewDate(10,5,2022);
        var date2 = DateUtils.NewDate(12,5,2022);
        when(task.getStartTime()).thenReturn(date);
        when(task.getEndTime()).thenReturn(date2);

        assertEquals(0, repo.getAll().size());
        service.AddTask("description", date, date2, 1);

        assertEquals(1, repo.getAll().size());
        var result = repo.getAll().get(0);
        assertEquals("description", result.getTitle());
        assertEquals(date, result.getStartTime());
        assertEquals(date2, result.getEndTime());
    }


    @Test
    void FilterTasksTest() throws Exception {
        when(task.getStartTime()).thenReturn(DateUtils.NewDate(10,5,2022));
        when(task.getEndTime()).thenReturn(DateUtils.NewDate(11,5,2022));
        when(task.nextTimeAfter(any(Date.class))).thenReturn(DateUtils.NewDate(11,5,2022));
        when(task2.getStartTime()).thenReturn(DateUtils.NewDate(13,5,2022));
        when(task2.getEndTime()).thenReturn(DateUtils.NewDate(14,5,2022));
        when(task2.nextTimeAfter(any(Date.class))).thenReturn(DateUtils.NewDate(14,5,2022));

        repo.add(task);
        repo.add(task2);

        var result = service.filterTasks(DateUtils.NewDate(9,5,2022), DateUtils.NewDate(12,5,2022));

        var resultList = StreamSupport.stream(result.spliterator(), false)
                .collect(Collectors.toList());

        assertEquals(1, resultList.size());
        assertTrue(resultList.contains(task));
    }

}
